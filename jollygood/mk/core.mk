ENABLE_AVX ?= 0
USE_EXTERNAL_TRIO ?= 0

LIBS_ICONV := -liconv
LIBS_PTHREAD := -lpthread
LIBS_WINMM := -lwinmm

BUILD = $(shell $(CC_FOR_BUILD) -dumpmachine)

CONF_ARGS = --build=$(BUILD) --host=$(HOST)

ifeq ($(ENABLE_AVX), 0)
	CONF_ARGS += --disable-avx
else
	CONF_ARGS += --enable-avx
endif

ifeq ($(PLATFORM), Windows)
	FLAGS += -fno-aggressive-loop-optimizations
	DEFS += -DWIN32 -D_LFS64_LARGEFILE=1
	LIBS += $(LIBS_WINMM)
else
	LIBS += $(LIBS_PTHREAD)
endif

ifeq ($(PLATFORM), Darwin)
	FLAGS += -stdlib=libc++
	DEFS += -DHAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE_NP
else ifeq ($(findstring $(PLATFORM),Linux NetBSD),)
	LIBS += $(LIBS_ICONV)
endif

ifeq ($(USE_EXTERNAL_TRIO), 0)
	CFLAGS_TRIO := -I$(SRCDIR)/trio
	LIBS_TRIO :=
	MKDIRS += trio
	SRCS_TRIO := trio/trio.c trio/trionan.c trio/triostr.c
	OBJS_TRIO := $(SRCS_TRIO:.c=.o)
else
	override REQUIRES_PRIVATE += libtrio
	CFLAGS_TRIO = $(shell $(PKG_CONFIG) --cflags libtrio)
	LIBS_TRIO = $(shell $(PKG_CONFIG) --libs libtrio)
	OBJS_TRIO :=
endif
